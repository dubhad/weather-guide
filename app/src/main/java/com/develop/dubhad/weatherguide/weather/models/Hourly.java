package com.develop.dubhad.weatherguide.weather.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Hourly {
    
    @SerializedName("data")
    private List<HourlyWeather> hourlyWeatherData;

    public List<HourlyWeather> getHourlyWeatherData() {
        return hourlyWeatherData;
    }
}
