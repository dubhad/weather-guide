package com.develop.dubhad.weatherguide.weather.util;

public enum TimeMode {
    DATE_TIME("dd MMMM yyyy HH:mm"),
    DATE("dd MMMM yyyy"),
    TIME("HH:mm"),
    DAY_MONTH("dd MMMM"),
    DAY_MONTH_TIME("dd MMMM HH:mm");

    private final String format;

    TimeMode(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }
}
