package com.develop.dubhad.weatherguide.weather.ui;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.develop.dubhad.weatherguide.R;
import com.develop.dubhad.weatherguide.weather.adapters.DailyWeatherAdapter;
import com.develop.dubhad.weatherguide.weather.models.DailyWeather;
import com.develop.dubhad.weatherguide.weather.models.Weather;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import static com.develop.dubhad.weatherguide.weather.ui.WeatherActivity.BASIC_VIEW_REFRESH_DELAY;

public class WeeklyWeatherFragment extends Fragment implements WeatherActivity.OptionsListener {

    private final SwipeRefreshLayout.OnRefreshListener refreshWeeklyWeatherListener = () ->
            ((WeatherActivity) requireActivity()).getLocation();

    private RecyclerView weeklyWeatherRecyclerView;
    private ProgressBar progressBar;
    private SwipeRefreshLayout refreshWeeklyWeatherView;

    private WeeklyWeatherListener weeklyWeatherListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        initFields();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_weekly_weather, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initWidgets(view);

        setupWeeklyWeatherRecyclerView();

        refreshWeeklyWeatherView.setColorSchemeResources(R.color.colorPrimary,
                R.color.colorPrimaryDark);
        refreshWeeklyWeatherView.setOnRefreshListener(refreshWeeklyWeatherListener);

        setupWeatherView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((WeatherActivity) requireActivity()).setUserWeatherListener(weeklyWeatherListener);
        ((WeatherActivity) requireActivity()).setOptionsListener(this);
    }

    @Override
    public void onPause() {
        refreshWeeklyWeatherView.setRefreshing(false);

        super.onPause();
    }

    @Override
    public void onUnitsChanged() {
        setupWeatherView();
    }

    private void setWeeklyWeatherView(Weather weather) {
        List<DailyWeather> dailyWeather = weather.getDaily().getDailyWeatherData();
        DailyWeatherAdapter adapter = new DailyWeatherAdapter(dailyWeather, weather.getTimezone());
        weeklyWeatherRecyclerView.setAdapter(adapter);
    }

    private void setupWeatherView() {
        Weather weather = ((WeatherActivity) requireActivity()).getLastKnownWeather();
        if (weather != null) {
            setWeeklyWeatherView(weather);
        } else {
            refreshWeeklyWeatherView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void initFields() {
        weeklyWeatherListener = new WeeklyWeatherListener();
    }

    private void initWidgets(View view) {
        weeklyWeatherRecyclerView = view.findViewById(R.id.rv_weekly_weather);
        progressBar = view.findViewById(R.id.progress_bar);
        refreshWeeklyWeatherView = view.findViewById(R.id.swipe_refresh_layout);
    }

    private void setupWeeklyWeatherRecyclerView() {
        weeklyWeatherRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        weeklyWeatherRecyclerView.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL);
        weeklyWeatherRecyclerView.addItemDecoration(itemDecoration);
    }


    private class WeeklyWeatherListener implements WeatherActivity.GetWeatherListener {

        @Override
        public void onGetWeather(Weather weather, String nearestPlace) {
            refreshWeeklyWeatherView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            refreshWeeklyWeatherView.setRefreshing(false);

            final Handler handler = new Handler();
            handler.postDelayed(() -> setWeeklyWeatherView(weather), BASIC_VIEW_REFRESH_DELAY);
        }

        @Override
        public void onFailure() {
            progressBar.setVisibility(View.GONE);
            refreshWeeklyWeatherView.setRefreshing(false);
        }
    }
}
