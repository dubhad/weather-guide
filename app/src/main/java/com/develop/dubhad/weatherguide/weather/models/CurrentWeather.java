package com.develop.dubhad.weatherguide.weather.models;

import com.google.gson.annotations.SerializedName;

public class CurrentWeather {

    @SerializedName("time")
    private long time;

    @SerializedName("summary")
    private String summary;

    @SerializedName("icon")
    private String iconDescription;

    @SerializedName("temperature")
    private float temperature;

    @SerializedName("apparentTemperature")
    private float apparentTemperature;

    @SerializedName("humidity")
    private float humidity;

    @SerializedName("pressure")
    private float pressure;

    @SerializedName("windSpeed")
    private float windSpeed;

    public long getTime() {
        return time;
    }

    public String getSummary() {
        return summary;
    }

    public String getIconDescription() {
        return iconDescription;
    }

    public float getTemperature() {
        return temperature;
    }

    public float getApparentTemperature() {
        return apparentTemperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }

    public float getWindSpeed() {
        return windSpeed;
    }
}
