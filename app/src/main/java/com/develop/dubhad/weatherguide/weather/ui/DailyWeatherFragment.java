package com.develop.dubhad.weatherguide.weather.ui;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.develop.dubhad.weatherguide.R;
import com.develop.dubhad.weatherguide.weather.adapters.HourlyWeatherAdapter;
import com.develop.dubhad.weatherguide.weather.models.DailyWeather;
import com.develop.dubhad.weatherguide.weather.models.Weather;
import com.develop.dubhad.weatherguide.weather.units.UnitsManager;
import com.develop.dubhad.weatherguide.weather.util.TimeMode;
import com.develop.dubhad.weatherguide.weather.util.WeatherConverter;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import static com.develop.dubhad.weatherguide.weather.ui.WeatherActivity.BASIC_VIEW_REFRESH_DELAY;


public class DailyWeatherFragment extends Fragment implements WeatherActivity.OptionsListener {

    private final SwipeRefreshLayout.OnRefreshListener refreshDailyWeatherListener = () ->
            ((WeatherActivity) requireActivity()).getLocation();

    private TextView summaryView;
    private TextView dateView;
    private ImageView iconView;
    private TextView sunriseTimeView;
    private TextView sunsetTimeView;
    private TextView minTempView;
    private TextView maxTempView;
    private TextView precipProbabilityView;
    private ImageView precipIconView;
    private TextView placeNameView;
    private ProgressBar progressBar;
    private LinearLayout hourlyWeatherView;
    private SwipeRefreshLayout refreshDailyWeatherView;

    private DailyWeatherListener dailyWeatherListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        initFields();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        return inflater.inflate(R.layout.fragment_daily_weather, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initWidgets(view);

        refreshDailyWeatherView.setColorSchemeResources(R.color.colorPrimary,
                R.color.colorPrimaryDark);
        refreshDailyWeatherView.setOnRefreshListener(refreshDailyWeatherListener);

        setupWeatherView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((WeatherActivity) requireActivity()).setUserWeatherListener(dailyWeatherListener);
        ((WeatherActivity) requireActivity()).setOptionsListener(this);
    }

    @Override
    public void onPause() {
        refreshDailyWeatherView.setRefreshing(false);

        super.onPause();
    }

    @Override
    public void onUnitsChanged() {
        setupWeatherView();
    }

    private void setDailyWeatherView(Weather weather) {
        DailyWeather todayWeather = weather.getDaily().getDailyWeatherData().get(0);

        String summary = WeatherConverter.convertSummary(todayWeather.getSummary());

        String date = WeatherConverter.convertTime(todayWeather.getTime(),
                weather.getTimezone(),
                TimeMode.DAY_MONTH);

        String sunriseTime = WeatherConverter.convertTime(todayWeather.getSunriseTime(),
                weather.getTimezone(),
                TimeMode.TIME);

        String sunsetTime = WeatherConverter.convertTime(todayWeather.getSunsetTime(),
                weather.getTimezone(),
                TimeMode.TIME);

        String minTemp = WeatherConverter.convertTemperature(todayWeather.getMinTemperature(),
                UnitsManager.getCurrentTemperatureUnit());

        String maxTemp = WeatherConverter.convertTemperature(todayWeather.getMaxTemperature(),
                UnitsManager.getCurrentTemperatureUnit());

        String precipProbability =
                WeatherConverter.convertPrecipProbability(todayWeather.getPrecipProbability());

        Resources resources = getResources();

        @DrawableRes int iconId = WeatherConverter.convertIcon(todayWeather.getIcon());
        Drawable icon = resources.getDrawable(iconId);

        @DrawableRes int precipIconId =
                WeatherConverter.convertPrecipIcon(todayWeather.getPrecipType());
        Drawable precipIcon = resources.getDrawable(precipIconId);

        summaryView.setText(summary);
        dateView.setText(date);
        iconView.setImageDrawable(icon);
        sunriseTimeView.setText(sunriseTime);
        sunsetTimeView.setText(sunsetTime);
        minTempView.setText(minTemp);
        maxTempView.setText(maxTemp);
        precipProbabilityView.setText(precipProbability);
        precipIconView.setImageDrawable(precipIcon);
    }

    private void setHourlyWeatherView(final Weather weather) {
        hourlyWeatherView.removeAllViews();

        HourlyWeatherAdapter adapter = new HourlyWeatherAdapter(requireContext(),
                weather.getHourly().getHourlyWeatherData(),
                weather.getTimezone());

        for (int i = 0; i < adapter.getCount(); i++) {
            @SuppressWarnings("ConstantConditions") final View item =
                    adapter.getView(i, null, null);

            hourlyWeatherView.addView(item);
        }
    }

    private void setupWeatherView() {
        Weather weather = ((WeatherActivity) requireActivity()).getLastKnownWeather();
        String placeName = ((WeatherActivity) requireActivity()).getLastKnownPlace();
        if (weather != null) {
            placeNameView.setText(placeName);
            setDailyWeatherView(weather);
            setHourlyWeatherView(weather);
        } else {
            refreshDailyWeatherView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void initFields() {
        dailyWeatherListener = new DailyWeatherListener();
    }

    private void initWidgets(View view) {
        summaryView = view.findViewById(R.id.text_summary);
        dateView = view.findViewById(R.id.text_date);
        placeNameView = view.findViewById(R.id.text_place_name);
        iconView = view.findViewById(R.id.image_icon);
        sunriseTimeView = view.findViewById(R.id.text_sunrise_time);
        sunsetTimeView = view.findViewById(R.id.text_sunset_time);
        minTempView = view.findViewById(R.id.text_temperature_min);
        maxTempView = view.findViewById(R.id.text_temperature_max);
        precipProbabilityView = view.findViewById(R.id.text_precipitation_probability);
        precipIconView = view.findViewById(R.id.image_precipitation_icon);
        progressBar = view.findViewById(R.id.progress_bar);
        hourlyWeatherView = view.findViewById(R.id.linear_layout_hourly_weather);
        refreshDailyWeatherView = view.findViewById(R.id.swipe_refresh_layout);
    }

    private class DailyWeatherListener implements WeatherActivity.GetWeatherListener {

        @Override
        public void onGetWeather(Weather weather, String nearestPlace) {
            refreshDailyWeatherView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            refreshDailyWeatherView.setRefreshing(false);

            final Handler handler = new Handler();
            handler.postDelayed(() -> {
                setDailyWeatherView(weather);
                setHourlyWeatherView(weather);
                placeNameView.setText(nearestPlace);
            }, BASIC_VIEW_REFRESH_DELAY);
        }

        @Override
        public void onFailure() {
            progressBar.setVisibility(View.GONE);
            refreshDailyWeatherView.setRefreshing(false);
        }
    }
}
