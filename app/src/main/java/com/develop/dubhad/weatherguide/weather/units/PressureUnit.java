package com.develop.dubhad.weatherguide.weather.units;

public enum PressureUnit {
    MILLIBARS("mb"),
    HECTOPASCALS("hPa");

    private final String symbol;

    PressureUnit(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
