package com.develop.dubhad.weatherguide.location;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.develop.dubhad.weatherguide.App;
import com.develop.dubhad.weatherguide.BuildConfig;

public final class SingleLocation {

    private static final String TAG = SingleLocation.class.getSimpleName();
    private static final int SECOND = 1000; //milliseconds
    private static final int WAIT_TIME = 5 * SECOND;
    private static final int KILOMETER = 1000; //meters
    private static final int ACCURACY = KILOMETER;

    private static LocationManager locationManager;
    private static SingleLocationListener singleLocationListener;
    private static NetworkLocationListener networkLocationListener;
    private static GpsLocationListener gpsLocationListener;
    private static Handler timerHandler;
    private static StopLocationUpdatesRunnable stopLocationUpdatesRunnable;
    private static boolean networkEnabled;
    private static boolean gpsEnabled;

    private SingleLocation() { }

    public static boolean requestLocation(SingleLocationListener listener) {
        singleLocationListener = listener;

        if (!isLocationAvailable()) {
            return false;
        }

        requestLocationUpdates();
        startLocationUpdatesTimer();

        return true;
    }

    public static void stopLocationUpdates() {
        cleanCallbacks();
    }

    private static boolean getLocationManager() {
        if (locationManager == null) {
            locationManager = (LocationManager) App.getContext()
                    .getSystemService(Context.LOCATION_SERVICE);
        }
        return locationManager != null;
    }

    private static boolean getProviders() {
        networkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return gpsEnabled || networkEnabled;
    }

    private static boolean isLocationAvailable() {
        return getLocationManager() && getProviders();
    }

    @SuppressLint("MissingPermission")
    private static void requestLocationUpdates() {
        if (networkEnabled) {
            if (networkLocationListener == null) {
                networkLocationListener = new NetworkLocationListener();
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    1000,
                    1,
                    networkLocationListener);
        }
        if (gpsEnabled) {
            if (gpsLocationListener == null) {
                gpsLocationListener = new GpsLocationListener();
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    1000,
                    1,
                    gpsLocationListener);
        }
    }

    private static void startLocationUpdatesTimer() {
        if (timerHandler == null) {
            timerHandler = new Handler();
        }
        if (stopLocationUpdatesRunnable == null) {
            stopLocationUpdatesRunnable = new StopLocationUpdatesRunnable();
        }
        timerHandler.removeCallbacks(stopLocationUpdatesRunnable);
        timerHandler.postDelayed(stopLocationUpdatesRunnable, WAIT_TIME);
        if (BuildConfig.DEBUG) Log.d(TAG, "Timer started");
    }

    @SuppressLint("MissingPermission")
    private static void getCachedLocation() {
        Location networkLocation = null;
        Location gpsLocation = null;
        if (networkEnabled) {
            networkLocation =
                    locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        if (gpsEnabled) {
            gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }

        if (networkLocation != null && gpsLocation != null) {
            if (networkLocation.getTime() > gpsLocation.getTime()) {
                singleLocationListener.onGetLocation(networkLocation);
            } else {
                singleLocationListener.onGetLocation(gpsLocation);
            }
            return;
        }
        if (networkLocation != null) {
            singleLocationListener.onGetLocation(networkLocation);
            return;
        }
        if (gpsLocation != null) {
            singleLocationListener.onGetLocation(gpsLocation);
            return;
        }
        singleLocationListener.onFailure();
    }

    private static void cleanListeners() {
        if (gpsLocationListener != null) {
            locationManager.removeUpdates(gpsLocationListener);
        }
        if (networkLocationListener != null) {
            locationManager.removeUpdates(networkLocationListener);
        }
    }

    private static void cleanCallbacks() {
        if (timerHandler != null) {
            timerHandler.removeCallbacks(stopLocationUpdatesRunnable);
            if (BuildConfig.DEBUG) Log.d(TAG, "Timer interrupted");
        }
        cleanListeners();
    }

    public interface SingleLocationListener {
        void onGetLocation(Location location);
        void onFailure();
    }

    private static class NetworkLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            if (location.getAccuracy() < ACCURACY) {
                cleanCallbacks();
                singleLocationListener.onGetLocation(location);
            }
        }

        public void onStatusChanged(String s, int i, Bundle bundle) { }

        public void onProviderEnabled(String s) { }

        public void onProviderDisabled(String s) { }
    }

    private static class GpsLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            if (location.getAccuracy() < ACCURACY) {
                cleanCallbacks();
                singleLocationListener.onGetLocation(location);
            }
        }

        public void onStatusChanged(String s, int i, Bundle bundle) { }

        public void onProviderEnabled(String s) { }

        public void onProviderDisabled(String s) { }
    }

    private static class StopLocationUpdatesRunnable implements Runnable {
        @Override
        public void run() {
            if (BuildConfig.DEBUG) Log.d(TAG, "Timer finished");
            cleanListeners();
            getCachedLocation();
        }
    }
}
