package com.develop.dubhad.weatherguide.weather.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.develop.dubhad.weatherguide.R;
import com.develop.dubhad.weatherguide.weather.models.HourlyWeather;
import com.develop.dubhad.weatherguide.weather.units.UnitsManager;
import com.develop.dubhad.weatherguide.weather.util.TimeMode;
import com.develop.dubhad.weatherguide.weather.util.WeatherConverter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class HourlyWeatherAdapter extends ArrayAdapter<HourlyWeather> {

    private final String timezone;

    private TextView tempView;
    private TextView timeView;
    private ImageView iconView;
    private TextView precipProbabilityView;
    private ImageView precipIconView;
    private TextView windDescriptionView;
    private TextView summaryView;

    public HourlyWeatherAdapter(Context context,
                                List<HourlyWeather> weatherData,
                                String timezone) {
        super(context, 0, weatherData);

        this.timezone = timezone;

        filterList(weatherData);
    }

    @NonNull
    @Override
    public View getView(int position,
                        @Nullable View convertView,
                        @SuppressWarnings("NullableProblems") @NonNull ViewGroup parent) {
        HourlyWeather weather = getItem(position);

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_hourly_weather, parent, false);
        }

        initViews(convertView);

        if (weather != null) {
            setupViews(convertView, weather);
        }

        return convertView;
    }

    private void initViews(View convertView) {
        tempView = convertView.findViewById(R.id.text_temperature);
        timeView = convertView.findViewById(R.id.text_time);
        iconView = convertView.findViewById(R.id.image_icon);
        precipProbabilityView = convertView.findViewById(R.id.text_precipitation_probability);
        precipIconView = convertView.findViewById(R.id.image_precipitation_icon);
        windDescriptionView = convertView.findViewById(R.id.text_wind_description);
        summaryView = convertView.findViewById(R.id.text_summary);
    }

    private void setupViews(View convertView, HourlyWeather weather) {
        String temp = WeatherConverter.convertTemperature(weather.getTemperature(),
                UnitsManager.getCurrentTemperatureUnit());

        String time = WeatherConverter.convertTime(weather.getTime(),
                timezone,
                TimeMode.TIME);

        String precipProbability =
                WeatherConverter.convertPrecipProbability(weather.getPrecipProbability());

        String windDescription = WeatherConverter.getWindDescription(weather.getWindSpeed());

        String summary = WeatherConverter.convertSummary(weather.getSummary());

        Resources resources = convertView.getResources();

        @DrawableRes int iconId = WeatherConverter.convertIcon(weather.getIconDescription());
        Drawable icon = resources.getDrawable(iconId);

        @DrawableRes int precipIconId = WeatherConverter.convertPrecipIcon(weather.getPrecipType());
        Drawable precipIcon = resources.getDrawable(precipIconId);

        tempView.setText(temp);
        timeView.setText(time);
        iconView.setImageDrawable(icon);
        precipProbabilityView.setText(precipProbability);
        precipIconView.setImageDrawable(precipIcon);
        windDescriptionView.setText(windDescription);
        summaryView.setText(summary);
    }

    private void filterList(List<HourlyWeather> weatherData) {
        Iterator<HourlyWeather> iter = weatherData.iterator();
        long todayTime = weatherData.get(0).getTime();
        while (iter.hasNext()) {
            HourlyWeather hw = iter.next();
            if (!isSameDay(todayTime, hw.getTime())) {
                iter.remove();
            }
        }
        for (int i = 1; i < weatherData.size(); i++) {
            weatherData.remove(i);
        }
    }

    private boolean isSameDay(long time1, long time2) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone(timezone));
        String sTime1 = sdf.format(new Date(time1 * 1000L));
        String sTime2 = sdf.format(new Date(time2 * 1000L));
        return sTime1.equals(sTime2);
    }
}
