package com.develop.dubhad.weatherguide.places;

public enum PlaceColumn {
    NAME("name"),
    LATITUDE("latitude"),
    LONGITUDE("longitude"),
    COUNTRY_NAME("countryName");

    private final String columnName;

    PlaceColumn(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }
}
