package com.develop.dubhad.weatherguide.weather.util;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;

import com.develop.dubhad.weatherguide.App;
import com.develop.dubhad.weatherguide.R;
import com.develop.dubhad.weatherguide.weather.units.PressureUnit;
import com.develop.dubhad.weatherguide.weather.units.SpeedUnit;
import com.develop.dubhad.weatherguide.weather.units.TemperatureUnit;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import androidx.annotation.DrawableRes;

public final class WeatherConverter {

    private WeatherConverter() { }

    public static String convertTemperature(float temperature, TemperatureUnit tempUnit) {
        String temperatureString;
        DecimalFormat df = new DecimalFormat("#");
        df.setRoundingMode(RoundingMode.CEILING);
        switch (tempUnit) {
            case FAHRENHEIT:
                temperatureString = df.format(temperature * 1.8 + 32);
                break;
            case KELVIN:
                temperatureString = df.format(temperature + 273.15);
                break;
            default:
                temperatureString = df.format(temperature);
                break;
        }
        temperatureString = temperatureString.replaceAll("^-(?=0?$)", "");
        temperatureString = temperatureString.concat(tempUnit.getSymbol());
        return temperatureString;
    }

    public static String convertSummary(String summary) {
        return summary;
    }

    public static String convertTime(long time, String timezone, TimeMode mode) {
        SimpleDateFormat sdf = new SimpleDateFormat(mode.getFormat(), Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone(timezone));
        return sdf.format(new Date(time * 1000L));
    }

    @DrawableRes
    public static int convertIcon(String iconDescription) {
        switch (iconDescription) {
            case "clear-day":
                return R.drawable.ic_wi_day_sunny;
            case "clear-night":
                return R.drawable.ic_wi_night_clear;
            case "rain":
                return R.drawable.ic_wi_rain;
            case "snow":
                return R.drawable.ic_wi_snow;
            case "sleet":
                return R.drawable.ic_wi_sleet;
            case "wind":
                return R.drawable.ic_wi_cloudy_windy;
            case "fog":
                return R.drawable.ic_wi_fog;
            case "cloudy":
                return R.drawable.ic_wi_cloudy;
            case "partly-cloudy-day":
                return R.drawable.ic_wi_day_cloudy;
            case "partly-cloudy-night":
                return R.drawable.ic_wi_night_alt_cloudy;
            default:
                return R.drawable.ic_wi_na;
        }
    }

    @DrawableRes
    public static int convertPrecipIcon(String precipType) {
        if (precipType == null) {
            return R.drawable.ic_wi_raindrop;
        }
        switch (precipType) {
            case "snow":
                return R.drawable.ic_wi_snowflake_cold;
            case "rain":
            case "sleet":
            default:
                return R.drawable.ic_wi_raindrop;
        }
    }

    public static String convertWindSpeed(float windSpeed, SpeedUnit speedUnit) {
        String windSpeedString;
        DecimalFormat df = new DecimalFormat("#");
        df.setRoundingMode(RoundingMode.CEILING);
        switch (speedUnit) {
            case KILOMETERS_HOUR:
                windSpeedString = df.format(windSpeed * 3.6);
                break;
            case MILES_HOUR:
                windSpeedString = df.format(windSpeed * 2.237);
                break;
            default:
                windSpeedString = df.format(windSpeed);
                break;
        }
        windSpeedString = windSpeedString.concat(" ").concat(speedUnit.getSymbol()).concat(",");
        return windSpeedString;
    }

    public static String convertHumidity(float humidity) {
        return String.format(Locale.getDefault(), "%d", (int) (humidity * 100))
                .concat("%");
    }

    public static String convertPrecipProbability(float precipProbability) {
        return String.format(Locale.getDefault(), "%d", (int) (precipProbability * 100))
                .concat("%");
    }

    public static String convertPressure(float pressure, PressureUnit pressureUnit) {
        String pressureString;
        DecimalFormat df = new DecimalFormat("#");
        df.setRoundingMode(RoundingMode.CEILING);
        switch (pressureUnit) {
            case MILLIBARS:
            default:
                pressureString = df.format(pressure);
                break;
        }
        pressureString = pressureString.concat(" ").concat(pressureUnit.getSymbol());
        return pressureString;
    }

    @SuppressLint("ResourceType")
    public static Drawable getWindIcon(float windSpeed) {
        Resources resources = App.getStaticResources();
        TypedArray beaufortScale = resources.obtainTypedArray(R.array.beaufort_scale);
        if (windSpeed < 0.5) {
            return beaufortScale.getDrawable(0);
        } else if (windSpeed >= 0.5 && windSpeed < 1.5) {
            return beaufortScale.getDrawable(1);
        } else if (windSpeed >= 1.5 && windSpeed < 3.3) {
            return beaufortScale.getDrawable(2);
        } else if (windSpeed >= 3.3 && windSpeed < 5.5) {
            return beaufortScale.getDrawable(3);
        } else if (windSpeed >= 5.5 && windSpeed < 7.9) {
            return beaufortScale.getDrawable(4);
        } else if (windSpeed >= 7.9 && windSpeed < 10.7) {
            return beaufortScale.getDrawable(5);
        } else if (windSpeed >= 10.7 && windSpeed < 13.8) {
            return beaufortScale.getDrawable(6);
        } else if (windSpeed >= 13.8 && windSpeed < 17.1) {
            return beaufortScale.getDrawable(7);
        } else if (windSpeed >= 17.1 && windSpeed < 20.7) {
            return beaufortScale.getDrawable(8);
        } else if (windSpeed >= 20.7 && windSpeed < 24.4) {
            return beaufortScale.getDrawable(9);
        } else if (windSpeed >= 24.4 && windSpeed < 28.4) {
            return beaufortScale.getDrawable(10);
        } else if (windSpeed >= 28.4 && windSpeed < 32.6) {
            return beaufortScale.getDrawable(11);
        } else if (windSpeed >= 32.6) {
            return beaufortScale.getDrawable(12);
        }
        beaufortScale.recycle();
        return resources.getDrawable(R.drawable.ic_wi_strong_wind);
    }

    public static String getWindDescription(float windSpeed) {
        Resources resources = App.getStaticResources();
        if (windSpeed < 0.5) {
            return resources.getString(R.string.beaufort_0);
        } else if (windSpeed >= 0.5 && windSpeed < 1.5) {
            return resources.getString(R.string.beaufort_1);
        } else if (windSpeed >= 1.5 && windSpeed < 3.3) {
            return resources.getString(R.string.beaufort_2);
        } else if (windSpeed >= 3.3 && windSpeed < 5.5) {
            return resources.getString(R.string.beaufort_3);
        } else if (windSpeed >= 5.5 && windSpeed < 7.9) {
            return resources.getString(R.string.beaufort_4);
        } else if (windSpeed >= 7.9 && windSpeed < 10.7) {
            return resources.getString(R.string.beaufort_5);
        } else if (windSpeed >= 10.7 && windSpeed < 13.8) {
            return resources.getString(R.string.beaufort_6);
        } else if (windSpeed >= 13.8 && windSpeed < 17.1) {
            return resources.getString(R.string.beaufort_7);
        } else if (windSpeed >= 17.1 && windSpeed < 20.7) {
            return resources.getString(R.string.beaufort_8);
        } else if (windSpeed >= 20.7 && windSpeed < 24.4) {
            return resources.getString(R.string.beaufort_9);
        } else if (windSpeed >= 24.4 && windSpeed < 28.4) {
            return resources.getString(R.string.beaufort_10);
        } else if (windSpeed >= 28.4 && windSpeed < 32.6) {
            return resources.getString(R.string.beaufort_11);
        } else if (windSpeed >= 32.6) {
            return resources.getString(R.string.beaufort_12);
        }
        return "";
    }

    public static String getPrecipDescription(float precipIntensity) {
        Resources resources = App.getStaticResources();
        if (precipIntensity < 2.5) {
            return resources.getString(R.string.precip_light);
        } else if (precipIntensity >= 2.5 && precipIntensity < 7.6) {
            return resources.getString(R.string.precip_moderate);
        } else if (precipIntensity >= 7.6 && precipIntensity < 50) {
            return resources.getString(R.string.precip_heavy);
        } else if (precipIntensity >= 50) {
            return resources.getString(R.string.precip_violent);
        }
        return "";
    }
}
