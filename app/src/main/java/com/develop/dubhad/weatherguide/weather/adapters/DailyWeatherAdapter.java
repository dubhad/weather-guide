package com.develop.dubhad.weatherguide.weather.adapters;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.develop.dubhad.weatherguide.R;
import com.develop.dubhad.weatherguide.weather.models.DailyWeather;
import com.develop.dubhad.weatherguide.weather.units.UnitsManager;
import com.develop.dubhad.weatherguide.weather.util.TimeMode;
import com.develop.dubhad.weatherguide.weather.util.WeatherConverter;

import java.util.List;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DailyWeatherAdapter extends RecyclerView.Adapter<DailyWeatherAdapter.ViewHolder> {

    private final List<DailyWeather> weatherData;
    private final String timezone;

    public DailyWeatherAdapter(List<DailyWeather> weatherData, String timezone) {
        this.weatherData = weatherData;
        this.timezone = timezone;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View dailyWeatherView =
                inflater.inflate(R.layout.item_daily_weather, parent, false);

        return new ViewHolder(dailyWeatherView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DailyWeather weather = weatherData.get(position);

        TextView dateView = holder.date;
        ImageView iconView = holder.icon;
        TextView minTempView = holder.tempMin;
        TextView maxTempView = holder.tempMax;
        TextView sunriseTimeView = holder.sunriseTime;
        TextView sunsetTimeView = holder.sunsetTime;
        ImageView precipIconView = holder.precipIcon;
        TextView precipProbabilityView = holder.precipProbability;
        TextView precipIntensityView = holder.precipIntensity;

        String date = WeatherConverter.convertTime(weather.getTime(),
                timezone,
                TimeMode.DAY_MONTH);

        String minTemp = WeatherConverter.convertTemperature(weather.getMinTemperature(),
                UnitsManager.getCurrentTemperatureUnit());

        String maxTemp = WeatherConverter.convertTemperature(weather.getMaxTemperature(),
                UnitsManager.getCurrentTemperatureUnit());

        String sunriseTime = WeatherConverter.convertTime(weather.getSunriseTime(),
                timezone,
                TimeMode.TIME);

        String sunsetTime = WeatherConverter.convertTime(weather.getSunsetTime(),
                timezone,
                TimeMode.TIME);

        float precipProbability = weather.getPrecipProbability();
        String precipProbabilityString =
                WeatherConverter.convertPrecipProbability(precipProbability);

        String precipIntensity =
                WeatherConverter.getPrecipDescription(weather.getPrecipIntensity());

        Resources resources = holder.itemView.getResources();

        @DrawableRes int iconId = WeatherConverter.convertIcon(weather.getIcon());
        Drawable icon = resources.getDrawable(iconId);

        @DrawableRes int precipIconId = WeatherConverter.convertPrecipIcon(weather.getPrecipType());
        Drawable precipIcon = resources.getDrawable(precipIconId);

        dateView.setText(date);
        iconView.setImageDrawable(icon);
        minTempView.setText(minTemp);
        maxTempView.setText(maxTemp);
        sunriseTimeView.setText(sunriseTime);
        sunsetTimeView.setText(sunsetTime);
        precipIconView.setImageDrawable(precipIcon);
        precipProbabilityView.setText(precipProbabilityString);
        precipIntensityView.setText(precipIntensity);

        if (precipProbability <= 0) {
            precipIntensityView.setText("–");
        }
    }

    @Override
    public int getItemCount() {
        return weatherData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final TextView date;
        final ImageView icon;
        final TextView tempMin;
        final TextView tempMax;
        final TextView sunriseTime;
        final TextView sunsetTime;
        final ImageView precipIcon;
        final TextView precipProbability;
        final TextView precipIntensity;

        ViewHolder(View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.text_date);
            icon = itemView.findViewById(R.id.image_icon);
            tempMin = itemView.findViewById(R.id.text_temperature_min);
            tempMax = itemView.findViewById(R.id.text_temperature_max);
            sunriseTime = itemView.findViewById(R.id.text_sunrise_time);
            sunsetTime = itemView.findViewById(R.id.text_sunset_time);
            precipIcon = itemView.findViewById(R.id.image_precipitation_icon);
            precipProbability = itemView.findViewById(R.id.text_precipitation_probability);
            precipIntensity = itemView.findViewById(R.id.text_precipitation_intensity);
        }
    }
}
