package com.develop.dubhad.weatherguide.weather.models;

import com.google.gson.annotations.SerializedName;

public class Weather {

    @SerializedName("timezone")
    private String timezone;

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("currently")
    private CurrentWeather currentWeather;

    @SerializedName("daily")
    private Daily daily;

    @SerializedName("hourly")
    private Hourly hourly;

    public String getTimezone() {
        return timezone;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public CurrentWeather getCurrentWeather() {
        return currentWeather;
    }

    public Daily getDaily() {
        return daily;
    }

    public Hourly getHourly() {
        return hourly;
    }
}
