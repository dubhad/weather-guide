package com.develop.dubhad.weatherguide.weather.network;

import com.develop.dubhad.weatherguide.weather.models.Weather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

interface DarkSkyInterface {

    @GET("/forecast/{appId}/{lat},{lon}?units=si")
    Call<Weather> weather(@Path("appId") String appId,
                          @Path("lat") double latitude,
                          @Path("lon") double longitude);
}
