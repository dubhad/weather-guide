package com.develop.dubhad.weatherguide.places;

import android.database.Cursor;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

@Dao
public interface PlaceDao {
    @Query("SELECT p.geoNameId AS _id, p.name, p.latitude, p.longitude, c.countryName " +
            "FROM Place p " +
            "JOIN Country c ON p.countryGeoNameId = c.countryGeoNameId " +
            "WHERE p.name LIKE :searchSequence " +
            "ORDER BY p.population DESC")
    Cursor getFilteredOrderedByPopulationPlacesCursor(String searchSequence);

    @Query("SELECT Place.name AS placeName, Country.countryName AS countryName " +
            "FROM Place, Country " +
            "WHERE Place.countryGeoNameId = Country.countryGeoNameId " +
            "ORDER BY ABS(latitude - :latitude) + ABS(longitude - :longitude) ASC " +
            "LIMIT 1")
    LiveData<PlaceCountry> getNearestPlace(double latitude, double longitude);

    class PlaceCountry {
        public String placeName;
        public String countryName;
    }
}
