package com.develop.dubhad.weatherguide.weather.ui;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.develop.dubhad.weatherguide.AboutDialogFragment;
import com.develop.dubhad.weatherguide.App;
import com.develop.dubhad.weatherguide.BuildConfig;
import com.develop.dubhad.weatherguide.R;
import com.develop.dubhad.weatherguide.location.SingleLocation;
import com.develop.dubhad.weatherguide.places.PlaceColumn;
import com.develop.dubhad.weatherguide.places.PlaceRepository;
import com.develop.dubhad.weatherguide.util.PermissionUtil;
import com.develop.dubhad.weatherguide.util.TabAdapter;
import com.develop.dubhad.weatherguide.weather.models.Weather;
import com.develop.dubhad.weatherguide.weather.network.WeatherRequest;
import com.develop.dubhad.weatherguide.weather.units.PressureUnit;
import com.develop.dubhad.weatherguide.weather.units.SpeedUnit;
import com.develop.dubhad.weatherguide.weather.units.TemperatureUnit;
import com.develop.dubhad.weatherguide.weather.units.UnitsManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;
import retrofit2.Response;

public class WeatherActivity extends AppCompatActivity {

    public static final int BASIC_VIEW_REFRESH_DELAY = 200;

    private static final String TAG = WeatherActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSION_ACCESS_FINE_LOCATION = 61125;
    private static final String FINE_LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String PREF_WEATHER = "PREF_WEATHER";
    private static final String PREF_PLACE = "PREF_PLACE";
    private static final String TAG_DIALOG_ABOUT = "TAG_DIALOG_ABOUT";

    private SearchView searchView;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private View parentLayout;

    private UserLocationListener userLocationListener;
    private Location userLocation;
    private UserWeatherRequestListener userWeatherRequestListener;
    private List<GetWeatherListener> getWeatherListeners;
    private List<OptionsListener> optionsListeners;
    private PlaceRepository placeRepository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        initFields();
        initWidgets();

        setupToolbar();
        initTabs();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (userLocation == null) {
            if (!PermissionUtil.hasPermission(this, FINE_LOCATION_PERMISSION)) {
                dealWithLocationPermission();
            } else {
                getLocation();
            }
        }
    }

    @Override
    protected void onPause() {
        WeatherRequest.cancelWeatherRequest();
        SingleLocation.stopLocationUpdates();

        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_weather, menu);

        setupUnitsOptionsMenu(menu);

        searchView = (SearchView) menu.findItem(R.id.menu_search_place).getActionView();

        SimpleCursorAdapter placesCursorAdapter = createPlacesAdapter();
        setupSearchView(placesCursorAdapter);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return setupOptionsBehaviour(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                }
            }
        }
    }

    public void getLocation() {
        if (!SingleLocation.requestLocation(userLocationListener)) {
            if (BuildConfig.DEBUG) Log.d(TAG, "Location services disabled");

            Snackbar snackbar = Snackbar.make(parentLayout,
                    getString(R.string.message_enable_location),
                    Snackbar.LENGTH_LONG);
            snackbar.show();
            sendFailureToWeatherListeners();
        }
    }

    public Weather getLastKnownWeather() {
        SharedPreferences sharedPref = App.getSharedPreferences();
        String jsonWeather = sharedPref.getString(PREF_WEATHER, "");

        Gson gson = new Gson();
        return gson.fromJson(jsonWeather, Weather.class);
    }

    public String getLastKnownPlace() {
        SharedPreferences sharedPref = App.getSharedPreferences();
        return sharedPref.getString(PREF_PLACE, "");
    }

    public void setUserWeatherListener(GetWeatherListener listener) {
        if (!getWeatherListeners.contains(listener)) {
            getWeatherListeners.add(listener);
        }
    }

    public void setOptionsListener(OptionsListener listener) {
        if (!optionsListeners.contains(listener)) {
            optionsListeners.add(listener);
        }
    }

    private void initFields() {
        userLocationListener = new UserLocationListener();
        userWeatherRequestListener = new UserWeatherRequestListener();

        userLocation = null;

        getWeatherListeners = new ArrayList<>();
        optionsListeners = new ArrayList<>();

        placeRepository = new PlaceRepository(getApplicationContext());
    }

    private void initWidgets() {
        toolbar = findViewById(R.id.toolbar);
        viewPager = findViewById(R.id.view_pager);
        tabLayout = findViewById(R.id.tab_layout);
        parentLayout = findViewById(android.R.id.content);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initTabs() {
        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment(new CurrentWeatherFragment(), getString(R.string.tab_now));
        adapter.addFragment(new DailyWeatherFragment(), getString(R.string.tab_day));
        adapter.addFragment(new WeeklyWeatherFragment(), getString(R.string.tab_week));
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void dealWithLocationPermission() {
        if (PermissionUtil.shouldShowRationale(this, FINE_LOCATION_PERMISSION)) {
            PermissionUtil.showRationaleAndRequestPermission(this,
                    FINE_LOCATION_PERMISSION,
                    REQUEST_PERMISSION_ACCESS_FINE_LOCATION,
                    getString(R.string.rationale_location));
        } else {
            PermissionUtil.requestPermission(this,
                    FINE_LOCATION_PERMISSION,
                    REQUEST_PERMISSION_ACCESS_FINE_LOCATION);
        }
    }

    private void sendFailureToWeatherListeners() {
        if (!getWeatherListeners.isEmpty()) {
            for (GetWeatherListener listener : getWeatherListeners) {
                listener.onFailure();
            }
        }
    }

    private void sendUnitsChangedToOptionsListeners() {
        if (!optionsListeners.isEmpty()) {
            for (OptionsListener listener : optionsListeners) {
                listener.onUnitsChanged();
            }
        }
    }

    private void showAboutDialog() {
        DialogFragment aboutDialogFragment = new AboutDialogFragment();
        aboutDialogFragment.show(getSupportFragmentManager(), TAG_DIALOG_ABOUT);
    }

    private SimpleCursorAdapter createPlacesAdapter() {
        SimpleCursorAdapter placesCursorAdapter =
                new SimpleCursorAdapter(WeatherActivity.this,
                        R.layout.item_suggestion,
                        null,
                        new String[]{PlaceColumn.NAME.getColumnName()},
                        new int[]{android.R.id.text1},
                        SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        placesCursorAdapter.setViewBinder((view, cursor, columnIndex) -> {
            int nameIndex = cursor.getColumnIndex(PlaceColumn.NAME.getColumnName());
            int countryNameIndex = cursor.getColumnIndex(PlaceColumn.COUNTRY_NAME.getColumnName());

            String placeName = cursor.getString(nameIndex);
            String countryName = cursor.getString(countryNameIndex);

            CharSequence rowContent = getString(R.string.place_row_content, placeName, countryName);
            ((TextView) view).setText(rowContent);
            return true;
        });

        placesCursorAdapter.setFilterQueryProvider(constraint ->
                placeRepository.getFilteredPlacesCursor((String) constraint));

        return placesCursorAdapter;
    }

    private void setupSearchView(final SimpleCursorAdapter adapter) {
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setSuggestionsAdapter(adapter);

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Cursor placesCursor = adapter.getCursor();
                placesCursor.moveToPosition(position);

                int nameIndex =
                        placesCursor.getColumnIndex(PlaceColumn.NAME.getColumnName());
                int countryNameIndex =
                        placesCursor.getColumnIndex(PlaceColumn.COUNTRY_NAME.getColumnName());
                int latitudeIndex =
                        placesCursor.getColumnIndex(PlaceColumn.LATITUDE.getColumnName());
                int longitudeIndex =
                        placesCursor.getColumnIndex(PlaceColumn.LONGITUDE.getColumnName());

                String placeName = placesCursor.getString(nameIndex);
                String countryName = placesCursor.getString(countryNameIndex);
                double latitude = placesCursor.getDouble(latitudeIndex);
                double longitude = placesCursor.getDouble(longitudeIndex);

                CharSequence query = getString(R.string.place_row_content, placeName, countryName);
                searchView.setQuery(query, true);

                getWeather(latitude, longitude);

                return true;
            }
        });
    }

    private void getWeather(double latitude, double longitude) {
        if (!WeatherRequest.requestWeather(latitude, longitude, userWeatherRequestListener)) {
            if (BuildConfig.DEBUG) Log.d(TAG, "Network services disabled");

            Snackbar snackbar = Snackbar.make(parentLayout,
                    getString(R.string.message_enable_network),
                    Snackbar.LENGTH_LONG);
            snackbar.show();

            sendFailureToWeatherListeners();
        }
    }

    private void getWeatherByLocation() {
        if (userLocation == null) {
            Snackbar snackbar = Snackbar.make(parentLayout,
                    getString(R.string.message_try_again),
                    Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
        getWeather(userLocation.getLatitude(), userLocation.getLongitude());
    }

    private void saveLastKnownWeather(Weather weather) {
        Gson gson = new Gson();
        String jsonWeather = gson.toJson(weather);

        SharedPreferences sharedPref = App.getSharedPreferences();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PREF_WEATHER, jsonWeather);
        editor.apply();
    }

    private void saveLastKnownPlace(String nearestPlace) {
        SharedPreferences sharedPref = App.getSharedPreferences();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PREF_PLACE, nearestPlace);
        editor.apply();
    }

    private void setupUnitsOptionsMenu(Menu menu) {
        TemperatureUnit currentTempUnit = UnitsManager.getCurrentTemperatureUnit();
        switch (currentTempUnit) {
            case CELSIUS:
                menu.findItem(R.id.menu_celsius_unit).setChecked(true);
                break;
            case FAHRENHEIT:
                menu.findItem(R.id.menu_fahrenheit_unit).setChecked(true);
                break;
            case KELVIN:
                menu.findItem(R.id.menu_kelvin_unit).setChecked(true);
                break;
        }

        SpeedUnit currentSpeedUnit = UnitsManager.getCurrentSpeedUnit();
        switch (currentSpeedUnit) {
            case METERS_SECOND:
                menu.findItem(R.id.menu_meters_second_unit).setChecked(true);
                break;
            case KILOMETERS_HOUR:
                menu.findItem(R.id.menu_kilometers_hour_unit).setChecked(true);
                break;
            case MILES_HOUR:
                menu.findItem(R.id.menu_miles_hour_unit).setChecked(true);
                break;
        }

        PressureUnit currentPressureUnit = UnitsManager.getCurrentPressureUnit();
        switch (currentPressureUnit) {
            case MILLIBARS:
                menu.findItem(R.id.menu_millibars_unit).setChecked(true);
                break;
            case HECTOPASCALS:
                menu.findItem(R.id.menu_hectopascals_unit).setChecked(true);
                break;
        }
    }

    private boolean setupOptionsBehaviour(MenuItem item) {
        item.setChecked(true);
        switch (item.getItemId()) {
            case R.id.menu_about:
                showAboutDialog();
                return true;
            case R.id.menu_celsius_unit:
                UnitsManager.saveTemperatureUnit(TemperatureUnit.CELSIUS);
                sendUnitsChangedToOptionsListeners();
                return true;
            case R.id.menu_fahrenheit_unit:
                UnitsManager.saveTemperatureUnit(TemperatureUnit.FAHRENHEIT);
                sendUnitsChangedToOptionsListeners();
                return true;
            case R.id.menu_kelvin_unit:
                UnitsManager.saveTemperatureUnit(TemperatureUnit.KELVIN);
                sendUnitsChangedToOptionsListeners();
                return true;
            case R.id.menu_meters_second_unit:
                UnitsManager.saveSpeedUnit(SpeedUnit.METERS_SECOND);
                sendUnitsChangedToOptionsListeners();
                return true;
            case R.id.menu_kilometers_hour_unit:
                UnitsManager.saveSpeedUnit(SpeedUnit.KILOMETERS_HOUR);
                sendUnitsChangedToOptionsListeners();
                return true;
            case R.id.menu_miles_hour_unit:
                UnitsManager.saveSpeedUnit(SpeedUnit.MILES_HOUR);
                sendUnitsChangedToOptionsListeners();
                return true;
            case R.id.menu_millibars_unit:
                UnitsManager.savePressureUnit(PressureUnit.MILLIBARS);
                sendUnitsChangedToOptionsListeners();
                return true;
            case R.id.menu_hectopascals_unit:
                UnitsManager.savePressureUnit(PressureUnit.HECTOPASCALS);
                sendUnitsChangedToOptionsListeners();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public interface OptionsListener {
        void onUnitsChanged();
    }

    public interface GetWeatherListener {
        void onGetWeather(Weather weather, String nearestPlace);
        void onFailure();
    }

    private class UserWeatherRequestListener implements WeatherRequest.WeatherRequestListener {

        private static final int WEATHER_LOADED_SNACKBAR_DELAY = 500;

        @Override
        public void onGetResponse(Response<Weather> response) {
            final Weather responseBody = response.body();
            if (responseBody == null) return;

            if (BuildConfig.DEBUG) Log.d(TAG, "Weather loaded");

            showWeatherLoadedSnackbar();

            placeRepository.getNearestPlace(responseBody.getLatitude(), responseBody.getLongitude())
                    .observe(WeatherActivity.this, place -> {
                        String nearestPlaceName =
                                getString(R.string.place_row_content,
                                        place.placeName,
                                        place.countryName);

                        saveLastKnownWeather(responseBody);
                        saveLastKnownPlace(nearestPlaceName);

                        if (!getWeatherListeners.isEmpty()) {
                            for (GetWeatherListener listener : getWeatherListeners) {
                                listener.onGetWeather(responseBody, nearestPlaceName);
                            }
                        }
                    });
        }

        @Override
        public void onFailure() {
            if (BuildConfig.DEBUG) Log.d(TAG, "Request failed");

            Snackbar snackbar = Snackbar.make(parentLayout,
                    getString(R.string.message_request_failed),
                    Snackbar.LENGTH_LONG);
            snackbar.show();

            sendFailureToWeatherListeners();
        }

        private void showWeatherLoadedSnackbar() {
            final Handler handler = new Handler();
            handler.postDelayed(() ->
                            Snackbar.make(parentLayout,
                                    getString(R.string.message_weather_loaded),
                                    Snackbar.LENGTH_SHORT)
                                    .show(),
                    WEATHER_LOADED_SNACKBAR_DELAY);
        }
    }

    private class UserLocationListener implements SingleLocation.SingleLocationListener {

        @Override
        public void onGetLocation(Location location) {
            Log.i(TAG, "Location: " + location.getLatitude() + " " + location.getLongitude());

            userLocation = location;
            getWeatherByLocation();
        }

        @Override
        public void onFailure() {
            if (BuildConfig.DEBUG) Log.d(TAG, "Unable to find location");

            Snackbar snackbar = Snackbar.make(parentLayout,
                    getString(R.string.message_unable_find_location),
                    Snackbar.LENGTH_LONG);
            snackbar.show();

            sendFailureToWeatherListeners();
        }
    }
}
