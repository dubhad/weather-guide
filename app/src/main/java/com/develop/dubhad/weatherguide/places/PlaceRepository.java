package com.develop.dubhad.weatherguide.places;

import android.content.Context;
import android.database.Cursor;

import com.fstyle.library.helper.AssetSQLiteOpenHelperFactory;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

public class PlaceRepository {

    private static final String DB_NAME = "db_places";

    private final PlaceDatabase placeDatabase;

    public PlaceRepository(Context context) {
        placeDatabase = Room.databaseBuilder(context, PlaceDatabase.class, DB_NAME)
                .openHelperFactory(new AssetSQLiteOpenHelperFactory())
                .build();
    }

    public Cursor getFilteredPlacesCursor(String searchSequence) {
        return placeDatabase.placeDao()
                .getFilteredOrderedByPopulationPlacesCursor(searchSequence + "%");
    }

    public LiveData<PlaceDao.PlaceCountry> getNearestPlace(double latitude, double longitude) {
        return placeDatabase.placeDao().getNearestPlace(latitude, longitude);
    }
}
