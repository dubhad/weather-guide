package com.develop.dubhad.weatherguide.places.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@SuppressWarnings("unused")
@Entity
public class Country {

    @PrimaryKey
    private int countryGeoNameId;
    private String countryCode;
    private String countryName;

    public int getCountryGeoNameId() {
        return countryGeoNameId;
    }

    public void setCountryGeoNameId(int countryGeoNameId) {
        this.countryGeoNameId = countryGeoNameId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
