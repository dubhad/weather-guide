package com.develop.dubhad.weatherguide.weather.models;

import com.google.gson.annotations.SerializedName;

public class DailyWeather {

    @SerializedName("summary")
    private String summary;

    @SerializedName("time")
    private long time;

    @SerializedName("icon")
    private String icon;

    @SerializedName("sunriseTime")
    private long sunriseTime;

    @SerializedName("sunsetTime")
    private long sunsetTime;

    @SerializedName("temperatureLow")
    private float minTemperature;

    @SerializedName("temperatureHigh")
    private float maxTemperature;

    @SerializedName("precipProbability")
    private float precipProbability;

    @SerializedName("precipType")
    private String precipType;

    @SerializedName("precipIntensity")
    private float precipIntensity;

    public String getSummary() {
        return summary;
    }

    public long getTime() {
        return time;
    }

    public String getIcon() {
        return icon;
    }

    public long getSunriseTime() {
        return sunriseTime;
    }

    public long getSunsetTime() {
        return sunsetTime;
    }

    public float getMinTemperature() {
        return minTemperature;
    }

    public float getMaxTemperature() {
        return maxTemperature;
    }

    public float getPrecipProbability() {
        return precipProbability;
    }

    public String getPrecipType() {
        return precipType;
    }

    public float getPrecipIntensity() {
        return precipIntensity;
    }
}
