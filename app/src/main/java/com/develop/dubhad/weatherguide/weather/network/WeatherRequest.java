package com.develop.dubhad.weatherguide.weather.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.develop.dubhad.weatherguide.App;
import com.develop.dubhad.weatherguide.weather.models.Weather;

import java.io.IOException;
import java.util.Properties;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class WeatherRequest {

    private static final String TAG = WeatherRequest.class.getSimpleName();
    private static final String API_KEY_FILE = "api_key.properties";

    private static WeatherRequestListener weatherRequestListener;
    private static WeatherResponseListener weatherResponseListener;
    private static DarkSkyInterface ds;
    private static Call<Weather> weatherCall;

    private WeatherRequest() { }

    public static boolean requestWeather(double latitude,
                                         double longitude,
                                         WeatherRequestListener listener) {
        weatherRequestListener = listener;

        if (!isNetworkAvailable()) {
            return false;
        }

        makeWeatherRequest(latitude, longitude);

        return true;
    }

    public static void cancelWeatherRequest() {
        if (weatherCall != null) {
            weatherCall.cancel();
        }
    }

    private static boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) App.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) {
            return false;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private static void makeWeatherRequest(double latitude, double longitude) {
        if (ds == null) {
            ds = RetrofitClientInstance.getRetrofitInstance()
                    .create(DarkSkyInterface.class);
        }
        if (weatherResponseListener == null) {
            weatherResponseListener = new WeatherResponseListener();
        }

        weatherCall = ds.weather(getApiKey(), latitude, longitude);
        weatherCall.enqueue(weatherResponseListener);
    }

    private static String getApiKey() {
        Properties properties = new Properties();
        try {
            properties.load(App.getContext()
                    .getAssets()
                    .open(API_KEY_FILE));
        } catch (IOException e) {
            Log.e(TAG, "API_KEY not found");
        }
        return properties.getProperty("API_KEY");
    }

    public interface WeatherRequestListener {
        void onGetResponse(Response<Weather> response);
        void onFailure();
    }

    private static class WeatherResponseListener implements Callback<Weather> {
        @Override
        public void onResponse(Call<Weather> call, Response<Weather> response) {
            Log.i(TAG, "Response: " + response.body());
            weatherRequestListener.onGetResponse(response);
        }

        @Override
        public void onFailure(Call<Weather> call, Throwable t) {
            Log.e(TAG, "Request to Dark Sky failed", t);
            weatherRequestListener.onFailure();
        }
    }
}
