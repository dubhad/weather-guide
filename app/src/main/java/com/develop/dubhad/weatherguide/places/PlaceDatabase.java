package com.develop.dubhad.weatherguide.places;

import com.develop.dubhad.weatherguide.places.models.Country;
import com.develop.dubhad.weatherguide.places.models.Place;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Place.class, Country.class}, version = 1, exportSchema = false)
public abstract class PlaceDatabase extends RoomDatabase {

    public abstract PlaceDao placeDao();
}
