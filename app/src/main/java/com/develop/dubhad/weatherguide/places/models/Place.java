package com.develop.dubhad.weatherguide.places.models;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@SuppressWarnings("unused")
@Entity(foreignKeys = @ForeignKey(entity = Country.class,
        parentColumns = "countryGeoNameId",
        childColumns = "countryGeoNameId",
        onDelete = ForeignKey.CASCADE))
public class Place {

    @PrimaryKey
    private int geoNameId;
    private String name;
    private float latitude;
    private float longitude;
    private int countryGeoNameId;
    private long population;

    public int getGeoNameId() {
        return geoNameId;
    }

    public void setGeoNameId(int geoNameId) {
        this.geoNameId = geoNameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public int getCountryGeoNameId() {
        return countryGeoNameId;
    }

    public void setCountryGeoNameId(int countryGeoNameId) {
        this.countryGeoNameId = countryGeoNameId;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }
}
