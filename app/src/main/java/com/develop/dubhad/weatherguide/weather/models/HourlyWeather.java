package com.develop.dubhad.weatherguide.weather.models;

import com.google.gson.annotations.SerializedName;

public class HourlyWeather {

    @SerializedName("temperature")
    private float temperature;

    @SerializedName("time")
    private long time;

    @SerializedName("icon")
    private String iconDescription;

    @SerializedName("precipProbability")
    private float precipProbability;

    @SerializedName("precipType")
    private String precipType;

    @SerializedName("windSpeed")
    private float windSpeed;

    @SerializedName("summary")
    private String summary;

    public float getTemperature() {
        return temperature;
    }

    public long getTime() {
        return time;
    }

    public String getIconDescription() {
        return iconDescription;
    }

    public float getPrecipProbability() {
        return precipProbability;
    }

    public String getPrecipType() {
        return precipType;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public String getSummary() {
        return summary;
    }
}
