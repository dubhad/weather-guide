package com.develop.dubhad.weatherguide.weather.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Daily {
    
    @SerializedName("data")
    private List<DailyWeather> dailyWeatherData;

    public List<DailyWeather> getDailyWeatherData() {
        return dailyWeatherData;
    }
}
