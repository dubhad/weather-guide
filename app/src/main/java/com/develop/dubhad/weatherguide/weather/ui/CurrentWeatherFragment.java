package com.develop.dubhad.weatherguide.weather.ui;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.develop.dubhad.weatherguide.R;
import com.develop.dubhad.weatherguide.weather.models.CurrentWeather;
import com.develop.dubhad.weatherguide.weather.models.Weather;
import com.develop.dubhad.weatherguide.weather.units.UnitsManager;
import com.develop.dubhad.weatherguide.weather.util.TimeMode;
import com.develop.dubhad.weatherguide.weather.util.WeatherConverter;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import static com.develop.dubhad.weatherguide.weather.ui.WeatherActivity.BASIC_VIEW_REFRESH_DELAY;

public class CurrentWeatherFragment extends Fragment implements WeatherActivity.OptionsListener {

    private final SwipeRefreshLayout.OnRefreshListener refreshCurrentWeatherListener = () ->
            ((WeatherActivity) requireActivity()).getLocation();

    private TextView timeView;
    private TextView summaryView;
    private TextView tempView;
    private ImageView iconView;
    private TextView apparentTempView;
    private TextView windSpeedView;
    private TextView windDescriptionView;
    private TextView humidityView;
    private TextView pressureView;
    private ImageView windIconView;
    private TextView placeNameView;
    private ProgressBar progressBar;
    private SwipeRefreshLayout refreshCurrentWeatherView;

    private CurrentWeatherListener currentWeatherListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        initFields();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        return inflater.inflate(R.layout.fragment_current_weather, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initWidgets(view);

        refreshCurrentWeatherView.setColorSchemeResources(R.color.colorPrimary,
                R.color.colorPrimaryDark);
        refreshCurrentWeatherView.setOnRefreshListener(refreshCurrentWeatherListener);

        setupWeatherView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((WeatherActivity) requireActivity()).setUserWeatherListener(currentWeatherListener);
        ((WeatherActivity) requireActivity()).setOptionsListener(this);
    }

    @Override
    public void onPause() {
        refreshCurrentWeatherView.setRefreshing(false);

        super.onPause();
    }

    @Override
    public void onUnitsChanged() {
        setupWeatherView();
    }

    private void setCurrentWeatherView(Weather weather) {
        CurrentWeather currentWeather = weather.getCurrentWeather();

        String time = WeatherConverter.convertTime(currentWeather.getTime(),
                weather.getTimezone(),
                TimeMode.DAY_MONTH_TIME);

        String summary = WeatherConverter.convertSummary(currentWeather.getSummary());

        String temp = WeatherConverter.convertTemperature(currentWeather.getTemperature(),
                UnitsManager.getCurrentTemperatureUnit());

        String apparentTemp =
                WeatherConverter.convertTemperature(currentWeather.getApparentTemperature(),
                        UnitsManager.getCurrentTemperatureUnit());

        String windSpeed = WeatherConverter.convertWindSpeed(currentWeather.getWindSpeed(),
                UnitsManager.getCurrentSpeedUnit());

        String windDescription = WeatherConverter.getWindDescription(currentWeather.getWindSpeed());

        String humidity = WeatherConverter.convertHumidity(currentWeather.getHumidity());

        String pressure = WeatherConverter.convertPressure(currentWeather.getPressure(),
                UnitsManager.getCurrentPressureUnit());

        Resources resources = getResources();

        @DrawableRes int iconId = WeatherConverter.convertIcon(currentWeather.getIconDescription());
        Drawable icon = resources.getDrawable(iconId);

        Drawable windIcon = WeatherConverter.getWindIcon(currentWeather.getWindSpeed());

        timeView.setText(time);
        summaryView.setText(summary);
        tempView.setText(temp);
        iconView.setImageDrawable(icon);
        apparentTempView.setText(apparentTemp);
        windSpeedView.setText(windSpeed);
        windDescriptionView.setText(windDescription);
        humidityView.setText(humidity);
        pressureView.setText(pressure);
        windIconView.setImageDrawable(windIcon);
    }

    private void setupWeatherView() {
        Weather weather = ((WeatherActivity) requireActivity()).getLastKnownWeather();
        String placeName = ((WeatherActivity) requireActivity()).getLastKnownPlace();
        if (weather != null) {
            setCurrentWeatherView(weather);
            placeNameView.setText(placeName);
        } else {
            refreshCurrentWeatherView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void initFields() {
        currentWeatherListener = new CurrentWeatherListener();
    }

    private void initWidgets(View view) {
        timeView = view.findViewById(R.id.text_time);
        summaryView = view.findViewById(R.id.text_summary);
        tempView = view.findViewById(R.id.text_temperature);
        iconView = view.findViewById(R.id.image_icon);
        apparentTempView = view.findViewById(R.id.text_apparent_temperature);
        windSpeedView = view.findViewById(R.id.text_wind_speed);
        windDescriptionView = view.findViewById(R.id.text_wind_description);
        humidityView = view.findViewById(R.id.text_humidity);
        pressureView = view.findViewById(R.id.text_pressure);
        windIconView = view.findViewById(R.id.image_wind_icon);
        placeNameView = view.findViewById(R.id.text_place_name);
        progressBar = view.findViewById(R.id.progress_bar);
        refreshCurrentWeatherView = view.findViewById(R.id.swipe_refresh_layout);
    }

    private class CurrentWeatherListener implements WeatherActivity.GetWeatherListener {

        @Override
        public void onGetWeather(Weather weather, String nearestPlace) {
            refreshCurrentWeatherView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            refreshCurrentWeatherView.setRefreshing(false);

            final Handler handler = new Handler();
            handler.postDelayed(() -> {
                setCurrentWeatherView(weather);
                placeNameView.setText(nearestPlace);
            }, BASIC_VIEW_REFRESH_DELAY);
        }

        @Override
        public void onFailure() {
            progressBar.setVisibility(View.GONE);
            refreshCurrentWeatherView.setRefreshing(false);
        }
    }
}
