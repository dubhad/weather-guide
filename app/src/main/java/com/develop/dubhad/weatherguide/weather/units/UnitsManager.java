package com.develop.dubhad.weatherguide.weather.units;

import android.content.SharedPreferences;

import com.develop.dubhad.weatherguide.App;

public final class UnitsManager {

    private static final String PREF_TEMP_UNIT = "PREF_TEMP_UNIT";
    private static final String PREF_SPEED_UNIT = "PREF_SPEED_UNIT";
    private static final String PREF_PRESSURE_UNIT = "PREF_PRESSURE_UNIT";

    private UnitsManager() { }

    public static void saveTemperatureUnit(TemperatureUnit unit) {
        SharedPreferences sharedPreferences = App.getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_TEMP_UNIT, unit.toString());
        editor.apply();
    }

    public static TemperatureUnit getCurrentTemperatureUnit() {
        SharedPreferences sharedPreferences = App.getSharedPreferences();
        String celsius = TemperatureUnit.CELSIUS.toString();
        String unit = sharedPreferences.getString(PREF_TEMP_UNIT, celsius);
        return TemperatureUnit.valueOf(unit);
    }

    public static void saveSpeedUnit(SpeedUnit unit) {
        SharedPreferences sharedPreferences = App.getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_SPEED_UNIT, unit.toString());
        editor.apply();
    }

    public static SpeedUnit getCurrentSpeedUnit() {
        SharedPreferences sharedPreferences = App.getSharedPreferences();
        String kilometersHour = SpeedUnit.KILOMETERS_HOUR.toString();
        String unit = sharedPreferences.getString(PREF_SPEED_UNIT, kilometersHour);
        return SpeedUnit.valueOf(unit);
    }

    public static void savePressureUnit(PressureUnit unit) {
        SharedPreferences sharedPreferences = App.getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_PRESSURE_UNIT, unit.toString());
        editor.apply();
    }

    public static PressureUnit getCurrentPressureUnit() {
        SharedPreferences sharedPreferences = App.getSharedPreferences();
        String hectopascals = PressureUnit.HECTOPASCALS.toString();
        String unit = sharedPreferences.getString(PREF_PRESSURE_UNIT, hectopascals);
        return PressureUnit.valueOf(unit);
    }
}
