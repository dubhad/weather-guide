package com.develop.dubhad.weatherguide.weather.units;

public enum SpeedUnit {
    METERS_SECOND("m/s"),
    KILOMETERS_HOUR("km/h"),
    MILES_HOUR("mph");

    private final String symbol;

    SpeedUnit(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
