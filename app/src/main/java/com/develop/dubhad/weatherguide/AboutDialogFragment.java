package com.develop.dubhad.weatherguide;

import android.app.Dialog;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class AboutDialogFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        LayoutInflater inflater = LayoutInflater.from(requireContext());
        View viewInflated =
                inflater.inflate(R.layout.dialog_about, (ViewGroup) getView(), false);

        TextView appVersion = viewInflated.findViewById(R.id.text_app_version);
        TextView poweredBy = viewInflated.findViewById(R.id.text_darksky_link);
        TextView developedBy = viewInflated.findViewById(R.id.text_dubhad_link);

        appVersion.setText(BuildConfig.VERSION_NAME);
        poweredBy.setMovementMethod(LinkMovementMethod.getInstance());
        developedBy.setMovementMethod(LinkMovementMethod.getInstance());

        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setView(viewInflated)
                .setTitle(getString(R.string.title_about))
                .setPositiveButton(android.R.string.ok, (dialog, which) -> { });

        return builder.create();
    }
}
