package com.develop.dubhad.weatherguide.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import com.develop.dubhad.weatherguide.R;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public final class PermissionUtil {

    private PermissionUtil() {}

    public static boolean hasPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermission(Activity activity, String permission, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
    }

    public static boolean shouldShowRationale(Activity activity, String permission) {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, permission);
    }

    public static void showRationaleAndRequestPermission(final Activity activity,
                                                         final String permission,
                                                         final int requestCode,
                                                         String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(activity.getString(R.string.title_permission_needed))
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, (dialog, which) ->
                        requestPermission(activity, permission, requestCode))
                .create()
                .show();
    }
}
