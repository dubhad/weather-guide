package com.develop.dubhad.weatherguide;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import java.lang.ref.WeakReference;

public class App extends Application {

    private static final String PREF_APP = "PREF_APP";

    private static WeakReference<Context> context;
    private static SharedPreferences sharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();

        context = new WeakReference<>(this.getApplicationContext());
        sharedPreferences = getSharedPreferences(PREF_APP, Context.MODE_PRIVATE);
    }

    public static Resources getStaticResources() {
        return getContext().getResources();
    }

    public static Context getContext() {
        return context.get();
    }

    public static SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }
}
